package test.java.com.arraylisthomework.services.arrays;

import main.java.com.arraylisthomework.arrays.ArrayListGeneric;
import main.java.com.arraylisthomework.arrays.ArrayListInt;
import main.java.com.arraylisthomework.interfaces.IListGeneric;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.NoSuchElementException;
import static org.junit.jupiter.api.Assertions.*;


class ArrayListIntTest {

    static Arguments [] clearTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(new int [] { 2, 4, 5, 6, 12, 4, 1, 23 }, new int [0]),
                    Arguments.arguments(new int [] { 5, 6, 7, 12, 4, 12, 3 }, new int [0]),
                    Arguments.arguments(new int [] {}, new int [0]),
                    Arguments.arguments(null, null)
            };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(int [] condition, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.clear();
        ArrayListInt expAsObj = new ArrayListInt(expected);
        Assertions.assertEquals(expAsObj.toArray(), cut.toArray());
    }

    static Arguments[] sizeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new int[] { 5, 4, 3, 12, 4, 51 }, 6),
                Arguments.arguments(new int[] { 5, 4, 3, 12 }, 4),
                Arguments.arguments(new int[] {}, 0),
                Arguments.arguments(null, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(int [] condition, int expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        Assertions.assertEquals(expected, cut.size());
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
          Arguments.arguments(new int [] { 25, 12, 44 }, new int [] { 25, 12, 44 }),
          Arguments.arguments(new int [] { 12, 44 }, new int [] { 12, 44 }),
          Arguments.arguments(new int [] {}, null),
          Arguments.arguments(null, null)
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int [] condition, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    static Arguments [] setTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int [] { 51, 12, 11, 21 }, 2, 13, new int [] { 51, 12, 13, 21 }),
                Arguments.arguments(new int [] { 55, 2, 0, 12 }, 1, 666, new int [] { 55, 666, 0, 12 }),
                Arguments.arguments(null, 1, 19, null),
                Arguments.arguments(new int [] { 15, 21, 3 }, 6, 19, new int [] { 15, 21, 3 })
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int [] condition, int index, int value, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.set(index, value);
        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    static Arguments[] addIndexAndValueTestArgs() {
        return new Arguments[]{
          Arguments.arguments(new int [] { 3, 4, 12, 24 }, 2, 241, new int [] { 3, 4, 241, 12, 24 }),
          Arguments.arguments(new int [] { 1, 2, 3}, 0, 12, new int [] { 12, 1, 2, 3 }),
          Arguments.arguments(new int [] { 1, 2, 3}, 2, 12, new int [] { 1, 2, 12, 3 }),
          Arguments.arguments(new int [] { 1, 2, 3}, 5, 12, new int [] { 1, 2, 3 }),
          Arguments.arguments(null, 2, 24, null)
        };
    }

    @ParameterizedTest
    @MethodSource ("addIndexAndValueTestArgs")
    void addIndexAndValueTest(int [] condition, int index, int value, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.add(index, value);
        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    static Arguments[] addTestArgs() {
        return new Arguments[]{
          Arguments.arguments(new int [] { 3, 4, 12 }, 666, new int [] { 3, 4, 12, 666 }),
          Arguments.arguments(new int [] { 12, 6, 7}, 111, new int [] { 12, 6, 7, 111 }),
          Arguments.arguments(new int [] { 1, 2 }, 12, new int [] { 1, 2, 12}),
          Arguments.arguments(new int [] {}, 5, null),
          Arguments.arguments(null, 5, null)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest (int [] condition, int addedElement, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.add(addedElement);
        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    static Arguments[] getTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new int [] { 5, 4, 3, 12, 4, 51}, 2, 3),
                Arguments.arguments(new int [] { 5, 4, 3, 12 }, 3, 12),
                Arguments.arguments(new int [] {}, 5, 0),
                Arguments.arguments(null, 0, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int [] condition, int targetIndex, int expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        Assertions.assertEquals(expected, cut.get(targetIndex));
    }


    static Arguments[] removeAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new int [] { 5, 6, 7, 12 }, new int [] { 5, 6 }, new int [] { 7, 12 }),
                Arguments.arguments(new int [] { 5, 6, 7, 12 }, new int [] { 5, 12 }, new int [] { 6, 7 }),
                Arguments.arguments(new int [] { 5, 6, 7, 12 }, new int [] { 5, 12, 665 }, new int [] { 6, 7 }),
                Arguments.arguments(new int [] { 5, 6, 7, 12 }, new int [] { 665, 42, 53 }, new int [] { 5, 6, 7, 12 }),
                Arguments.arguments(null, null, null)

        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAll (int [] condition, int [] toBeRemoved, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.removeAll(toBeRemoved);
        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    static Arguments[] removeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int [] { 666, 12, 41, 51, 22 }, 41, new int [] { 666, 12, 51, 22 }),
                Arguments.arguments(new int [] { 666, 12, 41, 51, 22 }, 22, new int [] { 666, 12, 41, 51 }),
                Arguments.arguments(new int [] { 666, 12, 41, 51, 22 }, 88, new int [] { 666, 12, 41, 51, 22 }),
                Arguments.arguments(null, 12, null)
        };
    }
    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int [] condition, int numberValue, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        cut.remove(numberValue);
        ArrayListInt res = new ArrayListInt(expected);
        Assertions.assertArrayEquals(res.toArray(), cut.toArray());
    }

    static Arguments[] containsCaseTrueTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int [] { 12, 543, 2, 33}, 33),
                Arguments.arguments(new int [] { 77, 43, 21, 776}, 776),
        };
    }

    @ParameterizedTest
    @MethodSource("containsCaseTrueTestArgs")
    void containsCaseTrue(int [] condition, int value) {
        ArrayListInt cut = new ArrayListInt(condition);
        Assertions.assertTrue(cut.contains(value));
    }

    static Arguments[] containsCaseFalseTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[] { 4, 6, 7, 12 }, 44),
                Arguments.arguments(new int[] { 6, 12, 532}, 122),
                Arguments.arguments(new int[] {}, 122),
                Arguments.arguments(null, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("containsCaseFalseTestArgs")
    void containsCaseFalse(int [] condition, int value) {
        ArrayListInt cut = new ArrayListInt(condition);
        Assertions.assertFalse(cut.contains(value));
    }


    static Arguments[] removeByIdTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int [] { 5, 12, 41, 777, 121, 3}, 2, new int [] { 5, 12, 777, 121, 3 }),
                Arguments.arguments(new int [] { 5, 12, 41, 777, 121, 3}, 2, new int [] { 5, 12, 777, 121, 3 }),
                Arguments.arguments(new int [] { 5, 12, 41, 777, 121, 3}, 3, new int [] { 5, 12, 41, 121, 3 }),
                Arguments.arguments(new int [] { 16, 24, 1, 132, 3}, 0, new int [] { 24, 1, 132, 3 }),
                Arguments.arguments(new int [] { 16, 24, 1, 132, 3}, 4, new int [] { 16, 24, 1, 132 }),
                Arguments.arguments(new int [] {}, 0, null),
                Arguments.arguments(null, 2, null),
                Arguments.arguments(new int [] { 5, 12, 41, 777, 121, 3}, 5, new int [] { 5, 12, 41, 777, 121 }),
                Arguments.arguments(new int [] { 5, 12, 41}, 5, new int [] { 5, 12, 41 })
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIdTestArgs")
    void removeByIdTest(int [] condition, int targetIndex, int [] expected) {
        ArrayListInt cut = new ArrayListInt(condition);
        ArrayListInt res = new ArrayListInt(expected);
        cut.removeById(targetIndex);
        Assertions.assertArrayEquals(res.toArray(), cut.toArray());
    }

    static class ArrayListGenericTest {

        private final IListGeneric<String> cut = new ArrayListGeneric<>();


        //    ****************************************************************
        static Arguments[] constructorsTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(new ArrayListGeneric<String>(), new String[]{}, 0),
                    Arguments.arguments(new ArrayListGeneric<String>(0), new String[]{}, 0),
                    Arguments.arguments(new ArrayListGeneric<String>(5), new String[]{}, 0),
                    Arguments.arguments(new ArrayListGeneric<String>(new String[]{"1", null, "2"}), new String[]{"1", null, "2"}, 3),
            };
        }


        @ParameterizedTest
        @MethodSource("constructorsTestArgs")
        void constructorTest(IListGeneric<String> cut, String[] expected, int expSize) {
            Object[] actual = cut.toArray();
            int actSize = cut.size();

            assertEquals(expSize, actSize);
            assertArrayEquals(expected, actual);
        }

        @Test
        void constructorsExceptionTest() {
            assertThrows(IllegalArgumentException.class, () -> new ArrayListGeneric<String>(-2));

            assertThrows(IllegalArgumentException.class, () -> new ArrayListGeneric<String>(null));
        }


        //    ****************************************************************
        static Arguments[] addNoIndexAndGetTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5),
                    Arguments.arguments(12)
            };
        }

        @ParameterizedTest
        @MethodSource("addNoIndexAndGetTestArgs")
        void addNoIndexAndGetTest(int countOfAdd) {
            String expected = "";
            for (int i = 0; i < countOfAdd; i++) {
                expected = String.valueOf(i);
                cut.add(expected);
            }

            String actual = cut.get(cut.size() - 1);

            assertEquals(expected, actual);
            assertEquals(countOfAdd, cut.size());
        }

        //    ****************************************************************
        static Arguments[] toArrayTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(2, new String[]{"0", "1"}),
                    Arguments.arguments(5, new String[]{"0", "1", "2", "3", "4"}),
                    Arguments.arguments(0, new String[]{}),
            };
        }

        @ParameterizedTest
        @MethodSource("toArrayTestArgs")
        void toArrayTest(int countAdd, String[] expected) {
            for (int i = 0; i < countAdd; i++) {
                cut.add(String.valueOf(i));
            }

            Object[] actual = cut.toArray();

            assertArrayEquals(expected, actual);
        }

        //    ****************************************************************
        @Test
        void clearTest() {
            int expSize = 0;
            cut.add("A");
            cut.add("B");
            cut.add("C");
            cut.add("D");

            cut.clear();

            assertEquals(expSize, cut.size());
            assertThrows(IndexOutOfBoundsException.class, () -> cut.get(0));
        }
    //    ****************************************************************

        static Arguments[] addIndexTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, 1, "AAA"),
                    Arguments.arguments(5, 5, "AAA"),
                    Arguments.arguments(5, 1, null),

            };
        }

        @ParameterizedTest
        @MethodSource("addIndexTestArgs")
        void addIndexTest(int countOfAdd, int index, String expected) {

            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }

            cut.add(index, expected);
            String actual = cut.get(index);

            assertEquals(expected, actual);
            assertEquals(countOfAdd + 1, cut.size());
        }

        static Arguments[] addIndexExceptionTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, 6, "AAA"),
                    Arguments.arguments(5, -1, "AAA"),
            };
        }

        @ParameterizedTest
        @MethodSource("addIndexExceptionTestArgs")
        void addIndexExceptionTest(int countOfAdd,  int index, String expected) {
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }

            assertThrows(IndexOutOfBoundsException.class, () -> cut.add(index, expected));

        }

        //    ****************************************************************
        static Arguments[] containsTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(true, "2", 5),
                    Arguments.arguments(false, "6", 5),
                    Arguments.arguments(false, "0", 0),
                    Arguments.arguments(false, null, 0),
                    Arguments.arguments(true, null, 5),
            };
        }

        @ParameterizedTest
        @MethodSource("containsTestArgs")
        void containsTest(boolean expected, String value, int countOfAdd) {
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(null);
                cut.add(String.valueOf(i));
            }

            boolean actual = cut.contains(value);

            assertEquals(expected, actual);
        }

    //    ****************************************************************

        static Arguments[] removeByIndexTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, 1),
                    Arguments.arguments(5, 4),
                    Arguments.arguments(5, 0),
            };
        }

        @ParameterizedTest
        @MethodSource("removeByIndexTestArgs")
        void removeByIndexTest(int countOfAdd, int index) {
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }
            int expSize = cut.size() - 1;
            String expRemovedValue = String.valueOf(index);


            String actRemovedValue = cut.removeByIndex(index);
            int actSize = cut.size();


            assertEquals(expRemovedValue, actRemovedValue);
            assertEquals(expSize, actSize);
        }

        static Arguments[] removeByIndexExceptionTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, 6),
                    Arguments.arguments(5, -1),
            };
        }

        @ParameterizedTest
        @MethodSource("removeByIndexExceptionTestArgs")
        void removeByIndexExceptionTest(int countOfAdd,  int index) {
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }

            assertThrows(IndexOutOfBoundsException.class, () -> cut.removeByIndex(index));
        }

        @Test
        void removeByIndexNullTest() {
            cut.add("0");
            cut.add(null);
            cut.add("1");
            int expSize = cut.size() - 1;
            String expRemovedValue = null;

            String actRemovedValue = cut.removeByIndex(1);
            int actSize = cut.size();

            assertEquals(expRemovedValue, actRemovedValue);
            assertEquals(expSize, actSize);
        }

    //    ****************************************************************

        static Arguments[] removeTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, "1"),
                    Arguments.arguments(5, "4"),
                    Arguments.arguments(5, null),
            };
        }

        @ParameterizedTest
        @MethodSource("removeTestArgs")
        void removeTest(int countOfAdd, String value) {
            cut.add(null);
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }
            int expSize = cut.size() - 1;


            String actRemovedValue = cut.remove(value);
            int actSize = cut.size();


            assertEquals(value, actRemovedValue);
            assertEquals(expSize, actSize);
        }

        @Test
        void removeExceptionTest() {

            assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));

            cut.add("BBB");

            assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));
        }

    //    ****************************************************************

        static Arguments[] setTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, 1, "AAA", true),
                    Arguments.arguments(5, 0, "AAA", true),
                    Arguments.arguments(5, 1, null, true),
            };
        }

        @ParameterizedTest
        @MethodSource("setTestArgs")
        void setTest(int countOfAdd, int index, String value, boolean expected) {
            cut.add(null);
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }
            int expSize = cut.size();
            boolean actualRez = cut.set(index, value);


            String actual = cut.get(index);
            int actSize = cut.size();

            assertEquals(expSize, actSize);
            assertEquals(value, actual);
            assertEquals(expected, actualRez);
        }

        @Test
        void setFalseTest() {

            assertFalse(cut.set(2, "AAA"));

            cut.add("Z");
            cut.add("X");
            cut.add("C");

            assertFalse(cut.set(3, "AAA"));
        }
    //    ****************************************************************

        static Arguments[] removeAllTestArgs() {
            return new Arguments[]{
                    Arguments.arguments(5, new String[]{"3", "1"}, new String[]{null, "0", "2", "4"}),
                    Arguments.arguments(5, new String[]{null, "1"}, new String[]{"0", "2", "3", "4"}),
                    Arguments.arguments(5, new String[]{"6", "1"}, new String[]{null, "0", "2", "3", "4"}),
                    Arguments.arguments(5, new String[]{"6", "99"}, new String[]{null, "0", "1", "2", "3", "4"}),
                    Arguments.arguments(5, new String[]{null, null, "99"}, new String[]{"0", "1", "2", "3", "4"}),
                    Arguments.arguments(5, new String[]{}, new String[]{null, "0", "1", "2", "3", "4"}),
                    Arguments.arguments(5, null, new String[]{null, "0", "1", "2", "3", "4"}),
            };
        }

        @ParameterizedTest
        @MethodSource("removeAllTestArgs")
        void removeAllTest(int countOfAdd, String[] removeValues, String[] expected) {
            cut.add(null);
            for (int i = 0; i < countOfAdd; i++) {
                cut.add(String.valueOf(i));
            }

            cut.removeAll(removeValues);

            assertArrayEquals(expected, cut.toArray());
        }

        @Test
        void removeAllRepeatedValueTest() {
            cut.add(null);
            cut.add(null);
            cut.add("A");
            cut.add("B");
            cut.add("B");
            cut.add("C");
            String[] expected = new String[]{null, "B", "C"};

            cut.removeAll(new String[]{ null, "A", "B"});

            assertArrayEquals(expected, cut.toArray());
        }
    }
}