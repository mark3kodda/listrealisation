package main.java.com.arraylisthomework.arrays;

import main.java.com.arraylisthomework.interfaces.IList;
import java.util.Arrays;

public class ArrayListInt implements IList {

    private int INITIAL_CAPACITY = 10;
    private int [] myArray;

    public ArrayListInt() {
      myArray = new int [INITIAL_CAPACITY];
    }

    public ArrayListInt(int capacity) {
        myArray = new int [capacity];
    }

    public ArrayListInt(int [] givenArray) {
        myArray = givenArray;
    }

    @Override
    public void clear() {
        myArray = new int [0];
    }

    @Override
    public int size() {
        return myArray.length;
    }

    @Override
    public int get(int index) {
        return myArray[index];
    }

    @Override
    public boolean add(int value) {
        if (checkIfNullOrZero()) {
            int [] tempArr = new int [myArray.length + 1];
            for (int i = 0; i < myArray.length; i++) {
                tempArr[i] = myArray[i];
            }
            tempArr[tempArr.length - 1] = value;
            myArray = tempArr;
            return true;
        }
        return false;
    }

    @Override
    public boolean add(int index, int value) {
        if (checkIfNullOrZero() && checkIfIndexInArr(index)) {
                int [] tempArr = new int [myArray.length + 1];
            if (index >= 0) {
                System.arraycopy(myArray, 0, tempArr, 0, index);
            }
                for (int i = index; i < tempArr.length; i++) {
                    if (i == index) {
                        tempArr[i] = value;
                    } else {
                        tempArr[i] = myArray[i - 1];
                    }
                }
                myArray = tempArr;
                return true;

        }
        return false;
    }

    @Override
    public int [] remove(int number) {
        if (checkIfNullOrZero()) {
            if (contains (number)) {
                int [] tempArr = new int [myArray.length - 1];
                int counter = 0;
                for (int value : myArray) {
                    if (value == number) {
                        continue;
                    }
                    tempArr[counter] = value;
                    counter++;
                }
                myArray = tempArr;
                return myArray;
            }
        }
        return myArray;
    }

    @Override
    public int [] removeById(int index) {
        if (checkIfNullOrZero()) {
            if (checkIfIndexInArr (index)) {
            int [] tempArr = new int [myArray.length - 1];
            int counter = 0;
            for (int i = 0; i < myArray.length; i++) {
                if (i == index) {
                    continue;
                }
                tempArr[counter] = myArray[i];
                counter++;
            }
            myArray = tempArr;
            return myArray;
        }
        }
        return null;
    }

    @Override
    public boolean contains(int value) {
        if (checkIfNullOrZero()) {
            for (int item : myArray) {
                if (item == value) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {
        if (checkIfIndexInArr (index)) {
            myArray[index] = value;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void print() {
        if (checkIfNullOrZero()) {
            System.out.print("[ ");
            for (int i = 0; i < myArray.length; i++) {
                if (i == myArray.length - 1) {
                    System.out.print(myArray[i] + " ");
                } else {
                    System.out.print(myArray[i] + ", ");
                }
            }
            System.out.print("]");
        }
    }

    @Override
    public int[] toArray() {
        if (checkIfNullOrZero()) {
            int [] tempArr = new int [myArray.length];
            for (int i = 0; i < myArray.length; i++) {
               tempArr[i] = myArray[i];
            }
            return tempArr;
        }
        return null;
    }

    @Override
    public boolean removeAll(int[] arrToRemove) {
        if (checkIfNullOrZero()) {
            for (int value : arrToRemove) {
                remove(value);
            }
            return true;
        }
        return false;
    }

    private boolean checkIfIndexInArr(int targetIndex) {
        return targetIndex >= 0 && targetIndex <= myArray.length;
    }

    private boolean checkIfNullOrZero() {
        if (myArray == null) {
            System.out.println("Array is null");
            return false;
        }
        if (myArray.length == 0) {
            System.out.println("Array with 0 length");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArrayListRealisationInt{" +
                "myArray=" + Arrays.toString(myArray) +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof ArrayListInt)) {
            System.out.println("Not same class");
            return false;
        }
        ArrayListInt objAsArr = (ArrayListInt) obj;
        if (myArray == objAsArr.myArray) {
            return true;
        }
        return super.equals(obj);
    }
}
