package main.java.com.arraylisthomework.interfaces;

public interface IList {

    void clear();
    int size();
    int get(int index);
    boolean add (int value);
    boolean add (int index, int value);
    int [] remove (int number);
    int [] removeById(int index);
    boolean contains (int value);
    boolean set(int index, int value);
    void print(); // выводит в консоль массив в квадрахных скобках, через запятую
    int [] toArray(); // приводит данные к массиву, в случае с AList ничего сложного у нас итак массив
    boolean removeAll(int []arr);

    /*
     1) по умолчанию капасити 10
     2) AList (int capacity)
     3) AList (int [] array )

    Реализации
     AList1 = хранит данные инты
     AList2 = хранит данные дженерики
     LList1 = однонаправленный
     LList2 = двунаправленный
     */



}
